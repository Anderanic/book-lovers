import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// Import Containers
import {
  FullLayoutComponent,
  SimpleLayoutComponent
} from './containers';
import { AuthGuard } from './service/common/authguard';

export const routes: Routes = [
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full',
  },
  {
    path: '',
    component: FullLayoutComponent, 
    canActivate: [AuthGuard],
    children: [
      {
        path: 'dashboard',
        loadChildren: './views/dashboard/dashboard.module#DashboardModule'
      },
      {
        path: 'search',
        loadChildren: './views/search/search.module#SearchModule'
      },
      {
        path: 'infobook',
        loadChildren: './views/info-book/info-book.module#InfoBookModule'
      }
    ]
  },
  {
    path: '',
    component: SimpleLayoutComponent,
    children: [
      {
        path: 'registration',
        loadChildren: './views/registration/registration.module#RegistrationModule',
      },
      {
        path: 'login',
        loadChildren: './views/login/login.module#LoginModule',
      },
      {
        path: 'recovery',
        loadChildren: './views/password-recovery/password-recovery.module#PasswordRecoveryModule',
      },
    ]
  },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule {}
