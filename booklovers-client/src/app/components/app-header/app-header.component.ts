import { Component, OnInit } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { UserDTO } from 'app/dto/UserDTO';
import { UserRepositoryService } from 'app/repository/user/user-repository.service';
import { MessageService } from 'app/service/message-handler/message.service';
import { environment } from 'environments/environment';
import { NavigationService } from 'app/service/navigation/navigation-service';
import { CookieService } from 'ngx-cookie-service';

@Component({
  selector: 'app-header',
  templateUrl: './app-header.component.html',
  styleUrls: ['./app-header.component.scss']
})
export class AppHeaderComponent implements OnInit{ 
  private currentTab: string;
  private titoloQuery: string = "";

  private DASHBOARD_TITLE: string = 'Dashboard';
  private FRIENDS_TITLE: string = 'Friends';
  private RECOMMENDED_TITLE: string = 'Recommended';

  private user: UserDTO;

  constructor(private router: Router,
              private navigationService: NavigationService,
              private userRepository: UserRepositoryService,
              private cookieService: CookieService,
              private messageService: MessageService){

                
    this.currentTab = this.DASHBOARD_TITLE;
  }

  ngOnInit(){
    this.router.events.subscribe((val) => {
      if(val instanceof NavigationStart)
        switch(val.url){
          case this.navigationService.DASHBOARD_URL: this.currentTab=this.DASHBOARD_TITLE;break;
          default: this.currentTab = "";
        } 
    });

    this.userRepository.getUserInfo()
                       .subscribe(user => {
                          this.user = user;
                          this.messageService.setUserInfo(user);
                        });
  }

  public cercaLibro(){
    this.navigationService.goToSearch(this.titoloQuery);
  }

  public logout(){
    //Richiamo la funzione di logout lato server dopo di che cancello il token e faccio il redirect alla pagina login
    this.userRepository.userLogout()
                       .subscribe(() => {
                          this.cookieService.delete("token");
                          this.navigationService.goToLogin();
                       }, err => {
                          console.log(err);
                       });
  }
}
