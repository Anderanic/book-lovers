import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { LibroDTO } from 'app/dto/LibroDTO';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { environment } from 'environments/environment';

@Component({
  selector: 'books-table',
  templateUrl: './books-table.component.html',
  styleUrls: ['./books-table.component.scss']
})
export class BooksTableComponent implements OnInit{

  @Input()  private tableTitle: string;
  @Input()  private tablePage: PagedResultDTO<LibroDTO>;
  @Input()  private rowsOnPage: number;
  @Input()  private showActions: boolean = true;

  @Output() private onTableChange = new EventEmitter<TableInfo>();
  @Output() private onItemClick = new EventEmitter<LibroDTO>();
  @Output() private onActionClickEvent = new EventEmitter<any>();
  
  private tableInfo: TableInfo;
  private tableAction = TableAction;

  constructor() {
      this.tableInfo = new TableInfo(environment.table);
  }

  ngOnInit(){
    //Se non passo nessun valore per rowsOnPage imposto il valore di default
    if(!this.rowsOnPage) this.rowsOnPage = this.tableInfo.rowsOnPage;    
  }

  public changePage(tableInfo: any): void{    
    //verifico se l'activepage è un numero per skippare il primo evento
    if(Number.isNaN(tableInfo.activePage)) return;

    //Imposto la currentPage mettendo -1 poichè la paginazione parte da pagina 0
    this.tableInfo.currentPage = tableInfo.activePage - 1; 
    this.tableInfo.rowsOnPage = tableInfo.rowsOnPage;   
    this.onTableChange.emit(this.tableInfo);
  }

  public changeSort(tableSort: any): void{
    //cambio il tipo di sort e propago l'evento
    this.tableInfo.sortBy = tableSort.sortBy;
    this.onTableChange.emit(this.tableInfo);
  }

  public onClick(libro: LibroDTO): void{
    //Al click di una riga propago l'evento con il libro selezionato
    this.onItemClick.emit(libro);
  }

  public onActionClick(action: TableAction, libro: LibroDTO){
    this.onActionClickEvent.emit({
      action: action,
      libro: libro
    });
  }
}

export class TableInfo{
  public rowsOnPage: number;
  public currentPage: number;
  public sortBy: string;
  public sortOrder: string;

  constructor(tableConfig: any){
    this.rowsOnPage = tableConfig.defaultSize;
    this.currentPage = tableConfig.startingPage;
    this.sortBy = tableConfig.startingSortBy;
    this.sortOrder = tableConfig.startingSortOrder;
  }
}

export enum TableAction{
  FAVOURITES, WISHLIST, READ, READING, DELETE
}