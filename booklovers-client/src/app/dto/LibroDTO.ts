export class LibroDTO{
    public titolo: string;
    public autore: string;
    public descrizione: string;
    public cover: string;
    public totPagine: number;
    public currentPage: number = 1;
    public isInLettura: boolean;
    public isLetto: boolean;
    public isPreferito: boolean;
    public isWishlist: boolean;
    public googleBooksId: string;

    public dataPubblicazione: string;
    public lingua: string;
    public googlePlayLink: string;
    public editore: string;
    public categoria: string;
    public rating: string;
    public prezzo: number;

    public constructor(googleBook?: any) {
        if(!googleBook) return this;
        
        this.titolo = googleBook.volumeInfo.title;
        this.autore = googleBook.volumeInfo.authors ? googleBook.volumeInfo.authors.toString() : null;
        this.descrizione = googleBook.volumeInfo.description;
        this.totPagine = googleBook.volumeInfo.pageCount;
        this.cover = googleBook.volumeInfo.imageLinks ? googleBook.volumeInfo.imageLinks.smallThumbnail : null;   
        this.googleBooksId = googleBook.id;

        this.dataPubblicazione = googleBook.volumeInfo.publishedDate;
        this.lingua = googleBook.volumeInfo.language;
        this.googlePlayLink = googleBook.volumeInfo.infoLink;
        this.editore = googleBook.volumeInfo.publisher;
        this.categoria = googleBook.volumeInfo.categories ? googleBook.volumeInfo.categories.toString() : null;
        this.rating = googleBook.volumeInfo.averageRating;
        this.prezzo = googleBook.saleInfo.listPrice ? googleBook.saleInfo.listPrice.amount : null;
    }
}