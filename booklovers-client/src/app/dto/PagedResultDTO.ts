export class PagedResultDTO<T>{
    public elements: T[];
    public totalElements: number;
    public totalPages: number;
}