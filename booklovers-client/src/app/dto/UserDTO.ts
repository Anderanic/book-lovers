
export class UserDTO{
    public nome: string;
    public cognome: string;
    public username: string;
    public email: string;
    public passwords: Passwords;
}

export class Passwords{

    constructor(password?: string, matchingPassword?: string){
        this.password = password;
        this.matchingPassword = matchingPassword;
    }

    public password: string;
    public matchingPassword: string;
}