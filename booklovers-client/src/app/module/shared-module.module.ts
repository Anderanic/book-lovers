import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LaddaModule } from 'angular2-ladda';
import { DataTableModule } from 'angular2-datatable';
import { ModalModule } from 'ngx-bootstrap/modal';
import { BsModalService } from 'ngx-bootstrap/modal/bs-modal.service';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { CookieService } from 'ngx-cookie-service';
import { ToasterModule, ToasterService} from 'angular2-toaster/angular2-toaster';
import { BsDatepickerModule, TabsModule } from '../../../node_modules/ngx-bootstrap';
import { UserRepositoryService } from '../repository/user/user-repository.service';
import { UtilService } from 'app/service/common/util.service';
import { LibriRepository } from 'app/repository/libri/libri-repository.service';
import { BooksTableComponent } from 'app/components/books-table/books-table.component';
import { GoogleBooksRepository } from 'app/repository/googlebooks/google-books-repository.service';
import { NavigationService } from 'app/service/navigation/navigation-service';

@NgModule({
  imports: [
    CommonModule,
    LaddaModule,
    DataTableModule,
    ModalModule,
    ChartsModule,
    ToasterModule,
    BsDatepickerModule.forRoot(),
    TabsModule.forRoot(),
  ],
  providers: [
    BsModalService,
    CookieService,
    ToasterService,
    UserRepositoryService,
    LibriRepository,
    UtilService,
    GoogleBooksRepository,
    NavigationService
  ],
  declarations: [BooksTableComponent],
  exports: [
    TabsModule,
    CommonModule,
    LaddaModule,
    DataTableModule,
    ModalModule,
    ChartsModule,
    ToasterModule,
    BooksTableComponent
  ]
})
export class SharedModule { }
