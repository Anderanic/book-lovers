import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { UserDTO } from 'app/dto/UserDTO';
import { UtilService } from 'app/service/common/util.service';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { LibroDTO } from 'app/dto/LibroDTO';
import { HttpErrorHandler } from 'app/service/common/http-error-handler';

@Injectable()
export class GoogleBooksRepository {

  constructor(private http: HttpClient,
              private utils: UtilService,
              private httpErrorHandler: HttpErrorHandler) { }

  public searchBooks(params: HttpParams): Observable<PagedResultDTO<any>>{
    return this.http.get(`${environment.server.url}/libri/google/search`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getBook(params: HttpParams): Observable<any>{
    return this.http.get(`${environment.server.url}/libri/google/get`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }
}
