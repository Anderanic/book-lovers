import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { UserDTO } from 'app/dto/UserDTO';
import { UtilService } from 'app/service/common/util.service';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { LibroDTO } from 'app/dto/LibroDTO';
import { HttpErrorHandler } from 'app/service/common/http-error-handler';

@Injectable()
export class LibriRepository {

  constructor(private http: HttpClient,
              private utils: UtilService,
              private httpErrorHandler: HttpErrorHandler) { }

  public getUserBook(params: HttpParams): Observable<LibroDTO>{
    return this.http.get(`${environment.server.url}/libri/user`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getPreferiti(params: HttpParams): Observable<PagedResultDTO<LibroDTO>>{
    return this.http.get(`${environment.server.url}/libri/user/preferiti`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getWishlist(params: HttpParams): Observable<PagedResultDTO<LibroDTO>>{
    return this.http.get(`${environment.server.url}/libri/user/wishlist`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getLetti(params: HttpParams): Observable<PagedResultDTO<LibroDTO>>{
    return this.http.get(`${environment.server.url}/libri/user/letti`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getInLettura(params: HttpParams): Observable<PagedResultDTO<LibroDTO>>{
    return this.http.get(`${environment.server.url}/libri/user/lettura`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public savePreferitiPreference(data: LibroDTO): Observable<null>{
    return this.http.post(`${environment.server.url}/libri/user/preference/preferiti`,data,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public saveWishlistPreference(data: LibroDTO): Observable<null>{
    return this.http.post(`${environment.server.url}/libri/user/preference/wishlist`,data,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public saveLettiPreference(data: LibroDTO): Observable<null>{
    return this.http.post(`${environment.server.url}/libri/user/preference/letti`,data,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public saveInLetturaPreference(data: LibroDTO): Observable<null>{
    return this.http.post(`${environment.server.url}/libri/user/preference/lettura`,data,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

}
