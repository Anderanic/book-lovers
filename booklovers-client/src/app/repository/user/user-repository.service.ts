import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';
import { Observable } from 'rxjs/Observable';
import { UserDTO, Passwords } from 'app/dto/UserDTO';
import { UtilService } from 'app/service/common/util.service';
import { LoginDTO } from 'app/dto/LoginDTO';
import { HttpErrorHandler } from 'app/service/common/http-error-handler';

@Injectable()
export class UserRepositoryService {

  constructor(private http: HttpClient,
              private utils: UtilService,
              private httpErrorHandler: HttpErrorHandler) { }

  public userRegistration(userDTO: UserDTO): Observable<UserDTO>{
    return this.http.post(`${environment.server.url}/user/registration`,userDTO,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public userLogin(loginDTO: LoginDTO): Observable<any>{
    let options = this.utils.createHeaderOptions();
    options.observe = 'response';
    return this.http.post(`${environment.server.url}/user/login`,loginDTO,options)
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public userLogout(): Observable<any>{
    return this.http.get(`${environment.server.url}/logout`, this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(e));
  }

  public checkUsername(username: string): Observable<Boolean>{
    let params = new HttpParams().set('username',username);
    return this.http.get(`${environment.server.url}/user/check/username`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public checkEmail(email: string): Observable<Boolean>{
    let params = new HttpParams().set('email',email);
    return this.http.get(`${environment.server.url}/user/check/email`,this.utils.createHeaderOptions(params))
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public getUserInfo(): Observable<UserDTO>{
    return this.http.get(`${environment.server.url}/user/logged/info`,this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public sendRecoveryEmail(userDTO: UserDTO): Observable<void>{
    return this.http.post(`${environment.server.url}/user/password/recovery`, userDTO, this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }

  public resetPassword(passwords: Passwords, token: string): Observable<void>{
    return this.http.post(`${environment.server.url}/user/password/reset/${token}`, passwords, this.utils.createHeaderOptions())
                    .catch((e: any) => Observable.throw(this.httpErrorHandler.handleError(e)));
  }
}
