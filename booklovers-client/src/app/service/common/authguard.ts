import { Injectable } from "@angular/core";
import { CanActivate, Router } from "@angular/router";
import { CookieService } from "ngx-cookie-service";
import { environment } from "environments/environment";

@Injectable()
export class AuthGuard implements CanActivate {

    base_url: string;

    constructor(private router: Router,
                private cookiService: CookieService) {}

    canActivate() {
        if (this.cookiService.check(environment.jwtTokenVariableName)) {
            return true;
        }

        this.router.navigate(['/login']);
        return false;
    }


}