import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { MessageService } from '../message-handler/message.service';

@Injectable()
export class HttpErrorHandler {

    private UNAUTHORIZED: number = 401;
    private FORBIDDEN: number = 403;
    private INTERNAL_SERVER_ERROR: number = 500;

    constructor(private router: Router,
                private cookieService: CookieService,
                private messageService: MessageService){}

    public handleError(error: any): any{
        if(error.status == this.UNAUTHORIZED){
            this.cookieService.delete(environment.jwtTokenVariableName);
            this.messageService.setSessionExpired(true);
            this.router.navigate(['login']);
        }
        return error.error;
    }
}
