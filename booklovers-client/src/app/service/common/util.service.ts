import { Injectable } from '@angular/core';
import { CookieService } from 'ngx-cookie-service';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { HttpHeaders, HttpParams } from '@angular/common/http';
import { environment } from 'environments/environment';

@Injectable()
export class UtilService {
  public toasterConfig : ToasterConfig = new ToasterConfig({
      tapToDismiss: true,
      timeout: 4000
    });

  constructor(private cookieService: CookieService) {}

  public getToasterConfig(): ToasterConfig{
    return this.toasterConfig;
  }

  public createHeaderOptions(params?: HttpParams): any{
    const headers = new HttpHeaders({
      'Content-Type': 'application/json',
      'authtoken': this.cookieService.get(environment.jwtTokenVariableName)
    });

    let options = {headers: headers, withCredentials: true, params: params};

    return options;
  }

  public showSuccessToastMessage(toasterService: ToasterService, title: string, message: string): void{
    toasterService.pop("success", title, message);
  }

  public showErrorToastMessage(toasterService: ToasterService, title: string, message: string): void{
    toasterService.pop("error", title, message ? message : "An error occurred");
  }
}
