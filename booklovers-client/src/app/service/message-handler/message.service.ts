import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { UserDTO } from 'app/dto/UserDTO';

@Injectable()
export class MessageService {
  private userInfoSource = new BehaviorSubject<UserDTO>(null);
  private sessionExpiredSource = new BehaviorSubject<boolean>(null);
  private registrationSuccessSource = new BehaviorSubject<boolean>(null);
  private successMessageSource = new BehaviorSubject<string>(null);

  public getUserInfoSource(): Observable<UserDTO>{
    return this.userInfoSource.asObservable();
  }

  public setUserInfo(user: UserDTO): void{
    if(user) this.userInfoSource.next(user);
  }

  public getUserInfo(): UserDTO{
    return this.userInfoSource.value;
  }

  public getSessionExpiredSource(): Observable<boolean>{
    return this.sessionExpiredSource.asObservable();
  }

  public setSessionExpired(sessionExpired: boolean): void{
    this.sessionExpiredSource.next(sessionExpired);
  }

  public getSessionExpired(): boolean{
    return this.sessionExpiredSource.value;
  }

  public getRegistrationSuccessSource(): Observable<boolean>{
    return this.registrationSuccessSource.asObservable();
  }

  public setRegistrationSuccess(sessionExpired: boolean): void{
    this.registrationSuccessSource.next(sessionExpired);
  }

  public getRegistrationSuccess(): boolean{
    return this.registrationSuccessSource.value;
  }

  public getSuccessMessageSource(): Observable<string>{
    return this.successMessageSource.asObservable();
  }

  public setSuccessMessage(message: string): void{
    this.successMessageSource.next(message);
  }

  public getSuccessMessage(): string{
    return this.successMessageSource.value;
  }
}
