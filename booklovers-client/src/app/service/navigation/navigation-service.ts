import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';

@Injectable()
export class NavigationService {
    public readonly INFOBOOK_URL: string = "/infobook";
    public readonly DASHBOARD_URL: string = "/dashboard";
    public readonly LOGIN_URL: string = "/login";
    public readonly REGISTRATION_URL: string = "/registration";
    public readonly SEARCH_URL: string = "/search";

    constructor(private router: Router){}

    public goToInfobook(googleBookId: string): void{
        let params = {};
        params[environment.urlParams.googleBookId] = googleBookId;
        this.router.navigate([this.INFOBOOK_URL], {queryParams: params});
    }

    public goToSearch(querySearch: string): void{
        let params = {};
        params[environment.urlParams.querySearch] = querySearch;
        this.router.navigate([this.SEARCH_URL], {queryParams: params});
    }

    public goToDashboard(): void{
        this.router.navigate([this.DASHBOARD_URL]);
    }

    public goToLogin(): void{
        this.router.navigate([this.LOGIN_URL]);
    }

    public goToRegistration(): void{
        this.router.navigate([this.REGISTRATION_URL]);
    }
}
