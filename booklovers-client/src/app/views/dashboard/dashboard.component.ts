import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { Router } from '@angular/router';
import { DashboardService } from './dashboard.service';
import { UtilService } from 'app/service/common/util.service';
import { UserDTO } from 'app/dto/UserDTO';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { LibroDTO } from 'app/dto/LibroDTO';
import { environment } from 'environments/environment';
import { TableInfo, TableAction } from 'app/components/books-table/books-table.component';
import { MessageService } from 'app/service/message-handler/message.service';
import { Subscription } from 'rxjs';
import { NavigationService } from 'app/service/navigation/navigation-service';
import { InfoBookService } from '../info-book/info-book.service';

@Component({
  templateUrl: 'dashboard.component.html',
  styleUrls: ['../../../scss/vendors/toastr/toastr.scss'],
  encapsulation: ViewEncapsulation.None  
})
export class DashboardComponent implements OnInit, OnDestroy{

  private user: UserDTO;
  private preferiti: PagedResultDTO<LibroDTO>;
  private wishlist: PagedResultDTO<LibroDTO>;
  private letti: PagedResultDTO<LibroDTO>;
  private inLettura: PagedResultDTO<LibroDTO>;

  private userInfoSubscription: Subscription;
  private tableAction = TableAction;

  constructor(private dashboardService: DashboardService,
              private infoBookService: InfoBookService,
              private utils: UtilService,
              private toasterService: ToasterService,
              private messageService: MessageService,
              private navigationService: NavigationService) {}

  ngOnInit(){
    //Nel momento in cui vengono recuperare le info utente le setto e inizializzo la dashboard
    this.userInfoSubscription = this.messageService.getUserInfoSource()
                                                   .subscribe(userDTO => {
                                                      if(userDTO) {
                                                        this.user = userDTO;
                                                        this.initDashboard();
                                                      }                                                      
                                                    }, err => {
                                                      this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                                                    });
  }

  ngOnDestroy(){
    if(this.userInfoSubscription) this.userInfoSubscription.unsubscribe();
  }

  public initDashboard(): void{
    //Creo l'oggetto per la paginazione dei risultati
    let tableInfo: TableInfo = new TableInfo(environment.table);

    //Recupero le liste dei libri collegati all'utente
    this.getPreferiti(tableInfo);
    this.getWishlist(tableInfo);
    this.getLetti(tableInfo);
    this.getInLettura(tableInfo);
  }

  public getPreferiti(tableInfo: TableInfo): void{    
    this.dashboardService.getPreferiti(this.user.username, tableInfo)
                         .subscribe(response => {
                            this.preferiti = response;
                         });
  }

  public getWishlist(tableInfo: TableInfo): void{
    this.dashboardService.getWishlist(this.user.username, tableInfo)
                         .subscribe(response => {
                            this.wishlist = response;
                         });
  }

  public getLetti(tableInfo: TableInfo): void{
    this.dashboardService.getLetti(this.user.username, tableInfo)
                         .subscribe(response => {
                            this.letti = response;
                         });
  }

  public getInLettura(tableInfo: TableInfo): void{
    this.dashboardService.getInLettura(this.user.username, tableInfo)
                         .subscribe(response => {
                            this.inLettura = response;
                         });
  }

  public openInfo(libro: LibroDTO): void{
    this.navigationService.goToInfobook(libro.googleBooksId);
  }

  public onActionClick(event: any, deleteAction: TableAction): void{
    this.infoBookService.setPreference(event.libro, event.action, deleteAction)
                         .subscribe(res => {
                           this.initDashboard();
                         }, err => {
                           this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                         });
  }
}
