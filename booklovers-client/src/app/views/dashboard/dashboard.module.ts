import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardService } from './dashboard.service';
import { SharedModule } from 'app/module/shared-module.module';
import { InfoBookService } from '../info-book/info-book.service';

@NgModule({
  imports: [
    DashboardRoutingModule,
    ChartsModule,
    SharedModule
  ],
  declarations: [ DashboardComponent ],
  providers: [DashboardService, InfoBookService]
})
export class DashboardModule { }
