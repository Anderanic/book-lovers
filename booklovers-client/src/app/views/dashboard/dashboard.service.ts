import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRepositoryService } from 'app/repository/user/user-repository.service';
import { LibriRepository } from 'app/repository/libri/libri-repository.service';
import { UserDTO } from 'app/dto/UserDTO';
import { HttpParams } from '@angular/common/http';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { LibroDTO } from 'app/dto/LibroDTO';
import { TableInfo, TableAction } from 'app/components/books-table/books-table.component';

@Injectable()
export class DashboardService {

  constructor(private userRepository: UserRepositoryService,
              private libriRepository: LibriRepository) {}

  public getUserInfo(): Observable<UserDTO>{
    return this.userRepository.getUserInfo();
  }

  public getPreferiti(username: string, tableInfo: TableInfo): Observable<PagedResultDTO<LibroDTO>>{
    return this.libriRepository.getPreferiti(this.getUserLibriParams(username, tableInfo));
  }

  public getWishlist(username: string, tableInfo: TableInfo): Observable<PagedResultDTO<LibroDTO>>{
    return this.libriRepository.getWishlist(this.getUserLibriParams(username, tableInfo));
  }

  public getLetti(username: string, tableInfo: TableInfo): Observable<PagedResultDTO<LibroDTO>>{
    return this.libriRepository.getLetti(this.getUserLibriParams(username, tableInfo));
  }

  public getInLettura(username: string, tableInfo: TableInfo): Observable<PagedResultDTO<LibroDTO>>{
    return this.libriRepository.getInLettura(this.getUserLibriParams(username, tableInfo));
  }  

  private getUserLibriParams(username: string, tableInfo: TableInfo): HttpParams{
    return new HttpParams().set('username',username)
                           .set('page', tableInfo.currentPage.toString())
                           .set('size', tableInfo.rowsOnPage.toString())
                           .set('sortBy', tableInfo.sortBy)
                           .set('sortOrder', tableInfo.sortOrder);
  } 

}
