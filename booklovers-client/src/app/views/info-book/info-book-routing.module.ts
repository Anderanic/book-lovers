import { NgModule } from '@angular/core';
import { Routes,
     RouterModule } from '@angular/router';
import { InfoBookComponent } from './info-book.component';


const routes: Routes = [
  {
    path: '',
    component: InfoBookComponent,
    data: {
      title: 'Info Book'
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class InfoBookRoutingModule {}
