import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { Subscription } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { environment } from 'environments/environment';
import { InfoBookService } from './info-book.service';
import { LibroDTO } from 'app/dto/LibroDTO';
import { UtilService } from 'app/service/common/util.service';
import { ToasterService } from 'angular2-toaster';
import { NavigationService } from 'app/service/navigation/navigation-service';
import { TableAction } from 'app/components/books-table/books-table.component';

@Component({
  selector: 'app-info-book',
  templateUrl: './info-book.component.html',
  styleUrls: ['./info-book.component.scss',
              '../../../scss/vendors/toastr/toastr.scss',],  
  encapsulation: ViewEncapsulation.None
})
export class InfoBookComponent implements OnInit {
  private subscription: Subscription;
  private libro: LibroDTO;
  private tableAction = TableAction;
  private currentPage: number = 1;

  constructor(private activatedRoute: ActivatedRoute,
              private infoBookService: InfoBookService,
              private utils: UtilService,
              private toasterService: ToasterService,
              private navigationService: NavigationService) {}

  ngOnInit() {    
    //Recupero il libro selezionato tramite il google book id passato nell'url
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      if(params[environment.urlParams.googleBookId]){
        this.infoBookService.getBook(params[environment.urlParams.googleBookId])
                            .subscribe(libro => {
                              this.libro = libro;

                              this.infoBookService.getUserBook(params[environment.urlParams.googleBookId])
                                                  .subscribe(libro => {
                                                    this.libro.isPreferito = libro.isPreferito;
                                                    this.libro.isWishlist = libro.isWishlist;
                                                    this.libro.isInLettura = libro.isInLettura;
                                                    this.libro.isLetto = libro.isLetto
                                                    if(libro.currentPage){
                                                      this.libro.currentPage = libro.currentPage
                                                    }                                                    
                                                  },
                                                  err => {
                                                    this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                                                  });
                            },
                            err => {
                              this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                            });                
      }
      else{
        this.navigationService.goToDashboard();
      }
    });
  }

  ngOnDestroy(){
    if(this.subscription) this.subscription.unsubscribe();
  }

  public openGooglePlay(): void{
    window.open(this.libro.googlePlayLink);
  }

  public addTo(action: TableAction){
    this.infoBookService.setPreference(this.libro, action)
                        .subscribe(() => {
                          this.utils.showSuccessToastMessage(this.toasterService, "Success!", "Book added to your collection");
                        }, err => {
                          console.log(err);
                        });
  }

}
