import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';

import { SharedModule } from 'app/module/shared-module.module';
import { InfoBookRoutingModule } from './info-book-routing.module';
import { InfoBookComponent } from './info-book.component';
import { InfoBookService } from './info-book.service';
import { FormsModule } from '@angular/forms';

@NgModule({
  imports: [
    InfoBookRoutingModule,
    ChartsModule,
    SharedModule,
    FormsModule
  ],
  declarations: [ InfoBookComponent ],
  providers: [InfoBookService]
})
export class InfoBookModule { }
