import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { GoogleBooksRepository } from 'app/repository/googlebooks/google-books-repository.service';
import { HttpParams } from '@angular/common/http';
import { LibroDTO } from 'app/dto/LibroDTO';
import { TableAction } from 'app/components/books-table/books-table.component';
import { LibriRepository } from 'app/repository/libri/libri-repository.service';

@Injectable()
export class InfoBookService {

  constructor(private googleBooksRepository: GoogleBooksRepository,
              private libriRepository: LibriRepository) {}

  public getBook(googleBookId: string): Observable<LibroDTO>{
    let params = new HttpParams().set('googleBookId', googleBookId);

    return this.googleBooksRepository.getBook(params)
                                     .map(googleBook => {
                                       return new LibroDTO(googleBook);
                                     });
  }

  public getUserBook(googleBookId: string): Observable<LibroDTO>{
    let params = new HttpParams().set('googleBookId', googleBookId);

    return this.libriRepository.getUserBook(params);
  }

  public setPreference(libro: LibroDTO, action: TableAction, deleteAction?: TableAction): Observable<null>{
    if(action == TableAction.DELETE){
      return this.savePreference(libro, deleteAction, false);
    }
    else{
      return this.savePreference(libro, action, true);
    }
  }

  private savePreference(libro: LibroDTO, action: TableAction, preference: boolean): Observable<null>{
    let response: Observable<null> = null;
    switch(action){
      case TableAction.FAVOURITES: libro.isPreferito = preference;
                                   response = this.libriRepository.savePreferitiPreference(libro);
                                   break;
      case TableAction.WISHLIST:   libro.isWishlist = preference;
                                   response = this.libriRepository.saveWishlistPreference(libro);
                                   break;
      case TableAction.READ:       libro.isLetto = preference;
                                   response = this.libriRepository.saveLettiPreference(libro);
                                   break;
      case TableAction.READING:    libro.isInLettura = preference;
                                   response = this.libriRepository.saveInLetturaPreference(libro);
                                   break;
    }

    return response;
  }
}
