import { Component, OnInit, ViewEncapsulation, AfterViewInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { LoginService } from './login.service';
import { ToasterConfig, ToasterService } from 'angular2-toaster';
import { UtilService } from 'app/service/common/util.service';
import { Router } from '@angular/router';
import { environment } from 'environments/environment';
import { CookieService } from 'ngx-cookie-service';
import { MessageService } from 'app/service/message-handler/message.service';
import { NavigationService } from 'app/service/navigation/navigation-service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['../../../scss/vendors/toastr/toastr.scss',
              './login.component.scss'],
  encapsulation: ViewEncapsulation.None
})
export class LoginComponent implements OnInit, AfterViewInit {

  private toasterConfig: ToasterConfig;

  profileForm = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),    
  });

  constructor(private loginService: LoginService,
              private toasterService: ToasterService,
              private utils: UtilService,
              private navigationService: NavigationService,
              private messageService: MessageService,
              private cookiService: CookieService) {     
    this.toasterConfig = this.utils.getToasterConfig();   
  }

  ngOnInit() {
    if(this.cookiService.check(environment.jwtTokenVariableName)){
      this.navigationService.goToDashboard();
    }    
  }

  ngAfterViewInit(){
    if(this.messageService.getSessionExpired()){
      this.utils.showErrorToastMessage(this.toasterService, "Session Expired!", "Please Sign In again");
    }

    if(this.messageService.getRegistrationSuccess()){
      this.messageService.setRegistrationSuccess(false);
      this.utils.showSuccessToastMessage(this.toasterService, environment.messages.successTitle, "Registration success!");
    }

    if(this.messageService.getSuccessMessage()){
      this.utils.showSuccessToastMessage(this.toasterService, environment.messages.successTitle, this.messageService.getSuccessMessage());      
      this.messageService.setSuccessMessage(null);
    }
  }

  public login(): void{
    if(this.profileForm.valid){
      this.loginService.login(this.profileForm.value)
                       .subscribe(response => {
                          this.messageService.setSessionExpired(false);
                          this.navigationService.goToDashboard();
                       }, err => {
                          this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                       });
    }    
  }
}
