import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserRepositoryService } from 'app/repository/user/user-repository.service';
import { LoginDTO } from 'app/dto/LoginDTO';
import { CookieService } from 'ngx-cookie-service';
import { environment } from 'environments/environment';

@Injectable()
export class LoginService {

  constructor(private userRepository: UserRepositoryService,
              private cookieService: CookieService) { }

  public login(loginDTO: LoginDTO): Observable<any>{
    return this.userRepository.userLogin(loginDTO)
                              .map(response => {
                                let token = response.headers.get("authtoken");
                                if(token != null){
                                  this.cookieService.set(environment.jwtTokenVariableName,token);
                                }
                              });
  }
}
