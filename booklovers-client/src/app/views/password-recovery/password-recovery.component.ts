import { Component, OnInit, ViewEncapsulation, OnDestroy } from '@angular/core';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { UtilService } from 'app/service/common/util.service';
import { Router, ActivatedRoute } from '@angular/router';
import { FormGroup, FormControl, Validators, FormGroupName } from '@angular/forms';
import { MessageService } from 'app/service/message-handler/message.service';
import { NavigationService } from 'app/service/navigation/navigation-service';
import { environment } from 'environments/environment';
import { UserRepositoryService } from 'app/repository/user/user-repository.service';
import { PasswordRecoveryService } from './password-recovery.service';
import { RegistrationService } from '../registration/registration.service';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-password-recovery',
  templateUrl: './password-recovery.component.html',  
  styleUrls: ['../../../scss/vendors/toastr/toastr.scss'],
  encapsulation: ViewEncapsulation.None
})
export class PasswordRecoveryComponent implements OnDestroy{

  private token: string = undefined;
  private email: string;
  private subscription: Subscription;

  profileForm = new FormGroup({
    passwords: new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      matchingPassword: new FormControl('', [Validators.required])
    }, {
      validators: this.registrationService.passwordConfirming
    })
  });

  constructor(private toasterService: ToasterService,
              private utils: UtilService,
              private activatedRoute: ActivatedRoute,
              private registrationService: RegistrationService,
              private passwordRecoveryService: PasswordRecoveryService,
              private messageService: MessageService,
              private navigationService: NavigationService) {

    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      if(params[environment.urlParams.token]){
        this.token = params[environment.urlParams.token];
      }
    });
  }

  ngOnDestroy(){
    this.subscription.unsubscribe();
  }

  public sendEmail(): void{
    this.passwordRecoveryService.sendEmail(this.email)
                                .subscribe(() => {
                                  this.messageService.setSuccessMessage(environment.messages.sendRecoveryEmail);
                                  this.navigationService.goToLogin();
                                }, err => {
                                  this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                                });
  }

  public resetPassword(): void{
    if(!this.token || this.token.length == 0) return;

    let password = this.profileForm.get('passwords').get('password').value;
    let matchingPassword = this.profileForm.get('passwords').get('matchingPassword').value;

    this.passwordRecoveryService.resetPassword(password, matchingPassword, this.token)
                                .subscribe(() => {
                                  this.messageService.setSuccessMessage(environment.messages.passwordReset);
                                  this.navigationService.goToLogin();
                                }, err => {
                                  this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                                });
  }
}
