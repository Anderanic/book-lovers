import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PasswordRecoveryComponent } from './password-recovery.component';
import { PasswordRecoveryRoutingModule } from './password-recovery-routing.module';
import { SharedModule } from 'app/module/shared-module.module';
import { PasswordRecoveryService } from './password-recovery.service';
import { RegistrationService } from '../registration/registration.service';

@NgModule({
  imports: [
    PasswordRecoveryRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ PasswordRecoveryComponent ],
  providers: [PasswordRecoveryService, RegistrationService]
})
export class PasswordRecoveryModule { }
