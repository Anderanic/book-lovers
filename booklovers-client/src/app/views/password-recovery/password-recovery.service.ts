import { Injectable } from "@angular/core";
import { Observable } from "rxjs";
import { UserRepositoryService } from "app/repository/user/user-repository.service";
import { Passwords, UserDTO } from "app/dto/UserDTO";

@Injectable()
export class PasswordRecoveryService {

    constructor(private userRepository: UserRepositoryService){}

    public sendEmail(email: string): Observable<void>{
        let userDTO: UserDTO = new UserDTO();
        userDTO.email = email;

        return this.userRepository.sendRecoveryEmail(userDTO);
    }

    public resetPassword(password: string, matchingPassword: string, token: string): Observable<void>{
        let passwords: Passwords = new Passwords(password, matchingPassword);

        return this.userRepository.resetPassword(passwords,token);
    }
}