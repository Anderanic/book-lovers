import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { RegistrationService } from './registration.service';
import { ToasterService, ToasterConfig } from 'angular2-toaster';
import { UtilService } from 'app/service/common/util.service';
import { Router } from '@angular/router';
import { FormGroup, FormControl, Validators, FormGroupName } from '@angular/forms';
import { MessageService } from 'app/service/message-handler/message.service';
import { NavigationService } from 'app/service/navigation/navigation-service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-registration',
  templateUrl: './registration.component.html',  
  styleUrls: ['../../../scss/vendors/toastr/toastr.scss'],
  encapsulation: ViewEncapsulation.None
})
export class RegistrationComponent {

  profileForm = new FormGroup({
    nome: new FormControl('', [Validators.required]),
    cognome: new FormControl('', [Validators.required]),
    username: new FormControl('',{
      validators: [Validators.required], 
      asyncValidators: this.registrationService.checkUsername().bind(this.registrationService),
      updateOn: 'blur'
    }),
    email: new FormControl('', {
      validators: [Validators.required, Validators.email], 
      asyncValidators: this.registrationService.checkEmail().bind(this.registrationService),
      updateOn: 'blur'
    }),
    passwords: new FormGroup({
      password: new FormControl('', [Validators.required, Validators.minLength(8)]),
      matchingPassword: new FormControl('', [Validators.required])
    }, {
      validators: this.registrationService.passwordConfirming
    })
  });

  constructor(private registrationService: RegistrationService,
              private toasterService: ToasterService,
              private utils: UtilService,
              private messageService: MessageService,
              private navigationService: NavigationService) {}

  public userRegistration(): void{
    console.log(this.profileForm.value);
    //Se la registrazione è andata a buon fine faccio il redirect alla pagina di login
    this.registrationService.userRegistration(this.profileForm.value)
                            .subscribe(response => {
                              this.messageService.setRegistrationSuccess(true);
                              this.navigationService.goToLogin();
                            },
                            err => {
                              this.utils.showErrorToastMessage(this.toasterService, environment.messages.errorTitle, err.error);
                            });
  }
}
