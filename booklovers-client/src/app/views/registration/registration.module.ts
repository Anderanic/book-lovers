import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RegistrationComponent } from './registration.component';
import { RegistrationRoutingModule } from './registration-routing.module';
import { RegistrationService } from './registration.service';
import { SharedModule } from 'app/module/shared-module.module';

@NgModule({
  imports: [
    RegistrationRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ RegistrationComponent ],
  providers: [RegistrationService]
})
export class RegistrationModule { }
