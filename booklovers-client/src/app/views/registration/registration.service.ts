import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { UserDTO } from 'app/dto/UserDTO';
import { UserRepositoryService } from 'app/repository/user/user-repository.service';
import { AbstractControl, AsyncValidatorFn } from '@angular/forms';
import { resolve } from 'url';
import { SummaryResolver } from '@angular/compiler';
import { map } from 'rxjs/operators';

@Injectable()
export class RegistrationService {

  constructor(private userRepository: UserRepositoryService) { }

  public userRegistration(user: UserDTO): Observable<any>{
    return this.userRepository.userRegistration(user);
  }

  public passwordConfirming(formControl: AbstractControl): any {
    if (formControl.get('password').value !== formControl.get('matchingPassword').value) {
        return {matching: true};
    }
  }

  public checkUsername(): AsyncValidatorFn {
    return (formControl: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.userRepository.checkUsername(formControl.value)
        .pipe(map(exists => {
            if(exists){
              return {usernameExists: true};
            }
            return null;            
          })
        );
    };
  }

  public checkEmail(): AsyncValidatorFn {
    return (formControl: AbstractControl): Observable<{ [key: string]: any } | null> => {
      return this.userRepository.checkEmail(formControl.value)
        .pipe(map(exists => {
          console.log(exists);
            if(exists){
              return {emailExists: true};
            }
            return null;            
          })
        );
    };
  }
}
