import { Component, OnInit, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { environment } from 'environments/environment';
import { Subscription } from 'rxjs';
import { SearchService } from './search.service';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { LibroDTO } from 'app/dto/LibroDTO';
import { TableInfo } from 'app/components/books-table/books-table.component';
import { UtilService } from 'app/service/common/util.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit, OnDestroy {

  private subscription: Subscription;
  private queryResult: PagedResultDTO<LibroDTO>;
  private queryTitle: string;

  readonly ROWS_ON_PAGE_SEARCH: number = 10;

  constructor(private activatedRoute: ActivatedRoute,
              private searchService: SearchService,
              private utils: UtilService,
              private router: Router) {}

  ngOnInit() {    
    this.subscription = this.activatedRoute.queryParams.subscribe(params => {
      if(params[environment.urlParams.querySearch]){
        this.queryTitle = params[environment.urlParams.querySearch];

        let tableInfo = new TableInfo(environment.table);
        tableInfo.rowsOnPage = this.ROWS_ON_PAGE_SEARCH;
        
        this.searchBooks(tableInfo);
      }
    });
  }

  ngOnDestroy(){
    if(this.subscription) this.subscription.unsubscribe();
  }

  public searchBooks(tableInfo: TableInfo): void{
    this.searchService.getBooks(this.queryTitle,tableInfo)
                      .subscribe(queryResult => {
                        this.queryResult = queryResult;
                      });
  }

  public openInfo(libro: LibroDTO){
    let queryParams = {};
    queryParams[environment.urlParams.googleBookId] = libro.googleBooksId;
    this.router.navigate(['infobook'], {queryParams: queryParams});
  }
}
