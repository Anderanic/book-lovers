import { NgModule } from '@angular/core';
import { ChartsModule } from 'ng2-charts/ng2-charts';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SearchComponent } from './search.component';
import { SearchRoutingModule } from './search-routing.module';
import { SearchService } from './search.service';
import { SharedModule } from 'app/module/shared-module.module';

@NgModule({
  imports: [
    SearchRoutingModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    SharedModule
  ],
  declarations: [ SearchComponent ],
  providers: [SearchService]
})
export class SearchModule { }
