import { Injectable } from '@angular/core';
import { PagedResultDTO } from 'app/dto/PagedResultDTO';
import { Observable } from 'rxjs';
import { LibroDTO } from 'app/dto/LibroDTO';
import { GoogleBooksRepository } from 'app/repository/googlebooks/google-books-repository.service';
import { HttpParams } from '@angular/common/http';
import { TableInfo } from 'app/components/books-table/books-table.component';

@Injectable()
export class SearchService {

  constructor(private googleBooksRepository: GoogleBooksRepository) { }

  public getBooks(titoloQuery: string, tableInfo: TableInfo): Observable<PagedResultDTO<LibroDTO>>{
    let params = new HttpParams().set('query', titoloQuery)
                                 .set('page', tableInfo.currentPage.toString())
                                 .set('size', tableInfo.rowsOnPage.toString());

    return this.googleBooksRepository.searchBooks(params)
                                     .map(googleBooks => {
                                        let pagedResult = new PagedResultDTO<LibroDTO>();

                                        pagedResult.totalElements = googleBooks.totalElements;
                                        pagedResult.totalPages = googleBooks.totalPages;
                                        pagedResult.elements = [];

                                        if(googleBooks.totalElements){
                                          googleBooks.elements.forEach(googleBook => {
                                            let libro = new LibroDTO(googleBook);
                                            pagedResult.elements.push(libro);
                                          });
                                        }
                                        
                                        
                                        return pagedResult;
                                      });
  }
}
