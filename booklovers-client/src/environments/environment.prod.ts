export const environment = {
  production: true,
  server: {
    url: 'http://localhost:8080',
    google: 'https://www.googleapis.com/books/v1/volumes'
  },
  code: {
    REGISTRATION_OK: "REGISTRATION_OK",
  },
  messages: {
    errorTitle: "Error!",
    successTitle: "Success!",
    sendRecoveryEmail: "An email was send to the specified address, check you email box!",
    passwordReset: "Your password has been changed!",
  },
  urlParams: {
    querySearch: 'q',
    googleBookId: 'gbid',
    token: 'token'
  },
  table: {
    startingPage: 0,
    defaultSize: 5,
    startingSortBy: "titolo",  
    startingSortOrder: "asc",
  },
  jwtTokenVariableName: "token",
};
