// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.

export const environment = {
  production: false,
  server: {
    url: 'http://localhost:8080',
    google: 'https://www.googleapis.com/books/v1/volumes'
  },
  messages: {
    errorTitle: "Error!",
    successTitle: "Success!",
    sendRecoveryEmail: "An email has been sent to the specified address, check your email box!",
    passwordReset: "Your password has been changed!",
  },
  urlParams: {
    querySearch: 'q',
    googleBookId: 'gbid',
    token: 'token',
  },
  table: {    
    startingPage: 0,
    defaultSize: 5,
    startingSortBy: "titolo",
    startingSortOrder: "asc",
  },
  jwtTokenVariableName: "token",
};
