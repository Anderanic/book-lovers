package it.booklovers.nicolosi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BookLoversServerApplication {

	public static void main(String[] args) {
		SpringApplication.run(BookLoversServerApplication.class, args);
	}

}
