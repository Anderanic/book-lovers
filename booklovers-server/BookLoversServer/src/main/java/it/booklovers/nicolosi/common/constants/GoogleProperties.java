package it.booklovers.nicolosi.common.constants;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Component;

import lombok.Getter;
import lombok.Setter;

@Configuration
@ConfigurationProperties("google")
@Component
@Getter
@Setter
public class GoogleProperties {
	private String apiKey;
}
