package it.booklovers.nicolosi.common.controller;

import javax.servlet.http.HttpSession;

import it.booklovers.nicolosi.common.constants.Constants;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.security.exception.SessionExpiredException;

public class BaseController extends ExceptionHandlerController {
	//Funzione per recuperare lo username dell'utente in sessione
	protected String getUsernameFromSession(HttpSession session) throws BaseException{
		if(session == null || session.getAttribute(Constants.USER_SESSION_ATTR) == null) throw new SessionExpiredException();
		
		return (String) session.getAttribute(Constants.USER_SESSION_ATTR);
	}
}
