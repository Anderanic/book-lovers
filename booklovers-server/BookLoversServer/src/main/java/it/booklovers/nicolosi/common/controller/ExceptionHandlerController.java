package it.booklovers.nicolosi.common.controller;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.ServletWebRequest;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import it.booklovers.nicolosi.common.dto.ErrorResponse;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.security.exception.SessionExpiredException;


@ControllerAdvice
public class ExceptionHandlerController extends ResponseEntityExceptionHandler {
	
    @ExceptionHandler(value = { BaseException.class })
    protected ResponseEntity<Object> handleConflict(BaseException ex, WebRequest request) {
        ErrorResponse errorResponse = new ErrorResponse(((ServletWebRequest)request).getRequest().getRequestURI().toString(), ex.getStatus().value(), ex.getErr());
        return handleExceptionInternal(ex, errorResponse, new HttpHeaders(), ex.getStatus(), request);
    }
}
