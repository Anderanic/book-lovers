package it.booklovers.nicolosi.common.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class ErrorResponse {

	public String path;
	public String error;
	public Integer status;	
	
	public ErrorResponse(String path, Integer status, String errorMessage) {
		this.path = path;
		this.status = status;
		this.error = errorMessage;
	}	
}
