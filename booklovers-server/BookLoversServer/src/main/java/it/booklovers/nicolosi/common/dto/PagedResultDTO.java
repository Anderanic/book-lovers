package it.booklovers.nicolosi.common.dto;

import java.util.List;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;

@Getter
@Setter
public class PagedResultDTO<T> {
	@NonNull
	private List<T> elements;	
	@NonNull
	private Long totalElements;	
	@NonNull
	private Integer totalPages;
	
	public PagedResultDTO(List<T> elements, Long totalElements, Integer totalPages) {
		this.elements = elements;
		this.totalElements = totalElements;
		this.totalPages = totalPages;		
	}
}
