package it.booklovers.nicolosi.common.exception;

import org.springframework.http.HttpStatus;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class BaseException extends Exception{
	private static final long serialVersionUID = 972900201803269016L;

	protected String err;
	protected HttpStatus status = HttpStatus.INTERNAL_SERVER_ERROR;
	
	public BaseException() {}
	
	public BaseException(String err) {
		this.err = err;
	}
}
