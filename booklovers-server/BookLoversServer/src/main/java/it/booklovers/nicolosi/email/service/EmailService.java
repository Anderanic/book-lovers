package it.booklovers.nicolosi.email.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.SimpleMailMessage;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.stereotype.Service;

import it.booklovers.nicolosi.common.exception.BaseException;

@Service
public class EmailService implements IEmailService {

	@Autowired
	private JavaMailSender emailSender;
	
	@Override
	public void sendResetPasswordEmail(String email, String token) throws BaseException {			    
		SimpleMailMessage message = constructEmail(RESET_PASSWORD_TITLE, RESET_PASSWORD_TEXT + PASSWORDRECOVERY_URL + token, email);
	    
	    emailSender.send(message);
	}
	 
	private SimpleMailMessage constructEmail(String subject, String body, String emailTo) {
	    SimpleMailMessage email = new SimpleMailMessage();
	    
	    email.setSubject(subject);
	    email.setText(body);
	    email.setTo(emailTo);
	    email.setFrom(EMAIL_SENDER);
	    
	    return email;
	}
}
