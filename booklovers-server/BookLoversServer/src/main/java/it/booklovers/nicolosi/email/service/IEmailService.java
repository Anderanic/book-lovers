package it.booklovers.nicolosi.email.service;

import it.booklovers.nicolosi.common.exception.BaseException;

public interface IEmailService {
	public final String EMAIL_SENDER = "noreply.booklovers@gmail.com";
	public final String PASSWORDRECOVERY_URL = "http://localhost:4200/#/recovery?token=";
	
	public final String RESET_PASSWORD_TITLE = "BookLovers Reset Password";
	public final String RESET_PASSWORD_TEXT = "Here the url to change your password. The following url is valid for 24 hours!\r\n\n";
	
	public void sendResetPasswordEmail(String email, String token) throws BaseException;
}
