package it.booklovers.nicolosi.entity;

import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 * The persistent class for the libri database table.
 * 
 */
@Entity
@NamedQuery(name="Libri.findAll", query="SELECT l FROM Libri l")
public class Libri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;
	
	@Column(name="google_books_id")
	private String googleBooksId;

	@Lob
	private String autore;

	@Lob
	private String cover;

	@Lob
	private String descrizione;

	@Lob
	private String titolo;

	@Column(name="tot_pagine")
	private Integer totPagine;

	//bi-directional many-to-one association to UserXLibri
	@OneToMany(mappedBy="libri")
	private List<UserXLibri> userXLibris;

	public Libri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getGoogleBooksId() {
		return googleBooksId;
	}

	public void setGoogleBooksId(String googleBooksId) {
		this.googleBooksId = googleBooksId;
	}

	public String getAutore() {
		return this.autore;
	}

	public void setAutore(String autore) {
		this.autore = autore;
	}

	public String getCover() {
		return this.cover;
	}

	public void setCover(String cover) {
		this.cover = cover;
	}

	public String getDescrizione() {
		return this.descrizione;
	}

	public void setDescrizione(String descrizione) {
		this.descrizione = descrizione;
	}

	public String getTitolo() {
		return this.titolo;
	}

	public void setTitolo(String titolo) {
		this.titolo = titolo;
	}

	public Integer getTotPagine() {
		return this.totPagine;
	}

	public void setTotPagine(Integer totPagine) {
		this.totPagine = totPagine;
	}

	public List<UserXLibri> getUserXLibris() {
		return this.userXLibris;
	}

	public void setUserXLibris(List<UserXLibri> userXLibris) {
		this.userXLibris = userXLibris;
	}

	public UserXLibri addUserXLibri(UserXLibri userXLibri) {
		getUserXLibris().add(userXLibri);
		userXLibri.setLibri(this);

		return userXLibri;
	}

	public UserXLibri removeUserXLibri(UserXLibri userXLibri) {
		getUserXLibris().remove(userXLibri);
		userXLibri.setLibri(null);

		return userXLibri;
	}

}