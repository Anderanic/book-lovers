package it.booklovers.nicolosi.entity;

import java.util.Date;
import java.io.Serializable;
import java.util.List;
import javax.persistence.*;

/**
 * The persistent class for the user database table.
 * 
 */
@Entity
@NamedQuery(name="User.findAll", query="SELECT u FROM User u")
public class User implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="codice_amico")
	private String codiceAmico;

	private String cognome;
	
	private String username;

	@Temporal(TemporalType.TIMESTAMP)
	@Column(name="data_creazione")
	private Date dataCreazione;

	private String email;

	private String nome;

	@Lob
	private String password;

	//bi-directional many-to-one association to UserXLibri
	@OneToMany(mappedBy="user", fetch = FetchType.EAGER)
	private List<UserXLibri> userXLibris;

	public User() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getCodiceAmico() {
		return this.codiceAmico;
	}

	public void setCodiceAmico(String codiceAmico) {
		this.codiceAmico = codiceAmico;
	}

	public String getCognome() {
		return this.cognome;
	}

	public void setCognome(String cognome) {
		this.cognome = cognome;
	}

	public Date getDataCreazione() {
		return this.dataCreazione;
	}

	public void setDataCreazione(Date dataCreazione) {
		this.dataCreazione = dataCreazione;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNome() {
		return this.nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getPassword() {
		return this.password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public List<UserXLibri> getUserXLibris() {
		return this.userXLibris;
	}

	public void setUserXLibris(List<UserXLibri> userXLibris) {
		this.userXLibris = userXLibris;
	}

	public UserXLibri addUserXLibri(UserXLibri userXLibri) {
		getUserXLibris().add(userXLibri);
		userXLibri.setUser(this);

		return userXLibri;
	}

	public UserXLibri removeUserXLibri(UserXLibri userXLibri) {
		getUserXLibris().remove(userXLibri);
		userXLibri.setUser(null);

		return userXLibri;
	}

}
