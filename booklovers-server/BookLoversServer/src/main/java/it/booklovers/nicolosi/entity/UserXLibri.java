package it.booklovers.nicolosi.entity;

import java.io.Serializable;
import javax.persistence.*;


/**
 * The persistent class for the user_x_libri database table.
 * 
 */
@Entity
@Table(name="user_x_libri")
@NamedQuery(name="UserXLibri.findAll", query="SELECT u FROM UserXLibri u")
public class UserXLibri implements Serializable {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name="current_page")
	private Integer currentPage;

	@Column(name="is_in_lettura")
	private Boolean isInLettura = Boolean.FALSE;

	@Column(name="is_letto")
	private Boolean isLetto = Boolean.FALSE;

	@Column(name="is_preferito")
	private Boolean isPreferito = Boolean.FALSE;

	@Column(name="is_wishlist")
	private Boolean isWishlist = Boolean.FALSE;

	//bi-directional many-to-one association to Libri
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_libro")
	private Libri libri;

	//bi-directional many-to-one association to User
	@ManyToOne(fetch = FetchType.EAGER)
	@JoinColumn(name="id_user")
	private User user;

	public UserXLibri() {
	}

	public Integer getId() {
		return this.id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getCurrentPage() {
		return this.currentPage;
	}

	public void setCurrentPage(Integer currentPage) {
		this.currentPage = currentPage;
	}

	public Boolean getIsInLettura() {
		return this.isInLettura;
	}

	public void setIsInLettura(Boolean isInLettura) {
		this.isInLettura = isInLettura;
	}

	public Boolean getIsLetto() {
		return this.isLetto;
	}

	public void setIsLetto(Boolean isLetto) {
		this.isLetto = isLetto;
	}

	public Boolean getIsPreferito() {
		return this.isPreferito;
	}

	public void setIsPreferito(Boolean isPreferito) {
		this.isPreferito = isPreferito;
	}

	public Boolean getIsWishlist() {
		return this.isWishlist;
	}

	public void setIsWishlist(Boolean isWishlist) {
		this.isWishlist = isWishlist;
	}

	public Libri getLibri() {
		return this.libri;
	}

	public void setLibri(Libri libri) {
		this.libri = libri;
	}

	public User getUser() {
		return this.user;
	}

	public void setUser(User user) {
		this.user = user;
	}

}
