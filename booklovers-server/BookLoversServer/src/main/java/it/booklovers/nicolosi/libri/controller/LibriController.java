package it.booklovers.nicolosi.libri.controller;

import javax.servlet.http.HttpSession;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.google.api.services.books.model.Volume;

import it.booklovers.nicolosi.common.controller.BaseController;
import it.booklovers.nicolosi.common.dto.PagedResultDTO;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.libri.dto.LibroDTO;
import it.booklovers.nicolosi.libri.service.ILibriService;
import it.booklovers.nicolosi.libri.service.ILibriService.UserLibriOption;

@RestController
@RequestMapping("libri")
public class LibriController extends BaseController {
	
	@Autowired
	private ILibriService libriService;
	
	@RequestMapping(path = "/user", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public LibroDTO getLibroUtente(HttpSession session,@RequestParam String googleBookId) throws BaseException {
		return libriService.getUserLibro(getUsernameFromSession(session), googleBookId);
	}
	
	@RequestMapping(path = "/user/preferiti", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public PagedResultDTO<LibroDTO> getLibriUserPreferiti(@RequestParam String username, @RequestParam(defaultValue="0") Integer page, @RequestParam(defaultValue="5") Integer size, 
														  @RequestParam(defaultValue="titolo") String sortBy, @RequestParam(defaultValue="asc") String sortOrder) throws BaseException {
		return libriService.getUserLibri(username, page, size, sortBy, sortOrder, UserLibriOption.PREFERITI);
	}
	
	@RequestMapping(path = "/user/wishlist", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public PagedResultDTO<LibroDTO> getLibrUserWishlist(@RequestParam String username, @RequestParam(defaultValue="0") Integer page, @RequestParam(defaultValue="5") Integer size, 
			  											@RequestParam(defaultValue="titolo") String sortBy, @RequestParam(defaultValue="asc") String sortOrder) throws BaseException {
		return libriService.getUserLibri(username, page, size, sortBy, sortOrder, UserLibriOption.WISHLIST);
	}
	
	@RequestMapping(path = "/user/letti", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public PagedResultDTO<LibroDTO> getLibriUserLetti(@RequestParam String username, @RequestParam(defaultValue="0") Integer page, @RequestParam(defaultValue="5") Integer size, 
			  										  @RequestParam(defaultValue="titolo") String sortBy, @RequestParam(defaultValue="asc") String sortOrder) throws BaseException {
		return libriService.getUserLibri(username, page, size, sortBy, sortOrder, UserLibriOption.LETTO);
	}
	
	@RequestMapping(path = "/user/lettura", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public PagedResultDTO<LibroDTO> getLibriUserInLettura(@RequestParam String username, @RequestParam(defaultValue="0") Integer page, @RequestParam(defaultValue="5") Integer size, 
														  @RequestParam(defaultValue="titolo") String sortBy, @RequestParam(defaultValue="asc") String sortOrder) throws BaseException {
		return libriService.getUserLibri(username, page, size, sortBy, sortOrder, UserLibriOption.IN_LETTURA);
	}
	
	@RequestMapping(path = "/google/search", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public PagedResultDTO<Volume> searchBooks(@RequestParam String query, @RequestParam(defaultValue="0") Long page, @RequestParam(defaultValue="10") Long size) throws BaseException{
		return libriService.searchBooks(query, page, size);
	}
	
	@RequestMapping(path = "/google/get", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public Volume getBook(@RequestParam String googleBookId) throws BaseException{
		return libriService.getBook(googleBookId);
	}
	
	@RequestMapping(path = "/user/preference/preferiti", method = RequestMethod.POST)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public void savePreferitiPreference(HttpSession session, @RequestBody LibroDTO libro) throws BaseException {		
		libriService.addUserPreference(getUsernameFromSession(session), libro, UserLibriOption.PREFERITI, libro.getIsPreferito());
	}
	
	@RequestMapping(path = "/user/preference/wishlist", method = RequestMethod.POST)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public void saveWishlistPreference(HttpSession session, @RequestBody LibroDTO libro) throws BaseException {		
		libriService.addUserPreference(getUsernameFromSession(session), libro, UserLibriOption.WISHLIST, libro.getIsWishlist());
	}
	
	@RequestMapping(path = "/user/preference/letti", method = RequestMethod.POST)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public void saveLettiPreference(HttpSession session, @RequestBody LibroDTO libro) throws BaseException {		
		libriService.addUserPreference(getUsernameFromSession(session), libro, UserLibriOption.LETTO, libro.getIsLetto());
	}
	
	@RequestMapping(path = "/user/preference/lettura", method = RequestMethod.POST)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public void saveLetturaPreference(HttpSession session, @RequestBody LibroDTO libro) throws BaseException {		
		libriService.addUserPreference(getUsernameFromSession(session), libro, UserLibriOption.IN_LETTURA, libro.getIsInLettura());
	}
}
