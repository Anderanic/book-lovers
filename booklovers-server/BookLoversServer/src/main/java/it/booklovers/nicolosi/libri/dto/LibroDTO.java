package it.booklovers.nicolosi.libri.dto;

import io.swagger.annotations.ApiModel;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@ApiModel
public class LibroDTO {
	private String titolo;
	private String autore;
	private String descrizione;
	private String cover;
	private Integer totPagine;
	private Integer currentPage;
	private Boolean isInLettura;
	private Boolean isLetto;
	private Boolean isPreferito;
	private Boolean isWishlist;
	private String googleBooksId;
	
	public LibroDTO() {}
	
	public LibroDTO(Boolean isUserBook) {
		this.isInLettura = isUserBook;
		this.isLetto = isUserBook;
		this.isPreferito = isUserBook;
		this.isWishlist = isUserBook;
	}
}
