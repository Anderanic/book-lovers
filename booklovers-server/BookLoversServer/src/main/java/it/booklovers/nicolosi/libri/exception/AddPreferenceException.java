package it.booklovers.nicolosi.libri.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class AddPreferenceException extends BaseException {
	public AddPreferenceException() {
		this.err = "An error occured while saving preference, please try again later.";
	}
}
