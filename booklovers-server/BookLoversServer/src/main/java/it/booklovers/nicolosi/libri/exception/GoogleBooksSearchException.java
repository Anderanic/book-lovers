package it.booklovers.nicolosi.libri.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class GoogleBooksSearchException extends BaseException {
	public GoogleBooksSearchException() {
		this.err = "An error occured while searching";
	}
	
	public GoogleBooksSearchException(String query) {
		this.err = "An error occured while searching: " + query;
	}
}
