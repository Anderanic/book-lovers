package it.booklovers.nicolosi.libri.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.booklovers.nicolosi.entity.Libri;

@Repository
public interface ILibriRepository extends CrudRepository<Libri, Integer> {
	public Optional<Libri> findByGoogleBooksId(String googleBooksId);
}