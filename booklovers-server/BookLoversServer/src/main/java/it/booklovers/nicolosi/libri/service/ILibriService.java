package it.booklovers.nicolosi.libri.service;

import com.google.api.services.books.model.Volume;

import it.booklovers.nicolosi.common.dto.PagedResultDTO;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.libri.dto.LibroDTO;

public interface ILibriService {
	public enum UserLibriOption {
		PREFERITI, WISHLIST, LETTO, IN_LETTURA;
	}
	
	public Integer GOOGLE_BOOKS_RESULT_LIMIT = 100;
	
	public LibroDTO getUserLibro(String username, String googleBooksId) throws BaseException;
	public PagedResultDTO<LibroDTO> getUserLibri(String username, Integer page, Integer size, String sortBy, String sortOrder, UserLibriOption option) throws BaseException;
	public PagedResultDTO<Volume> searchBooks(String query, Long page, Long size) throws BaseException;
	public Volume getBook(String googleBookId) throws BaseException;
	public void addUserPreference(String username, LibroDTO libro, UserLibriOption option,  Boolean preference) throws BaseException;
}
