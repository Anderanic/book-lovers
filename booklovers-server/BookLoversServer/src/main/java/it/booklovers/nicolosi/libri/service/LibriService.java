package it.booklovers.nicolosi.libri.service;

import java.io.IOException;
import java.security.GeneralSecurityException;
import java.util.List;
import java.util.Optional;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import com.google.common.reflect.TypeToken;

import it.booklovers.nicolosi.common.constants.GoogleProperties;
import it.booklovers.nicolosi.common.dto.PagedResultDTO;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.entity.Libri;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.entity.UserXLibri;
import it.booklovers.nicolosi.libri.dto.LibroDTO;
import it.booklovers.nicolosi.libri.exception.AddPreferenceException;
import it.booklovers.nicolosi.libri.exception.GoogleBooksSearchException;
import it.booklovers.nicolosi.libri.repository.ILibriRepository;
import it.booklovers.nicolosi.user.exception.UserNotFoundException;
import it.booklovers.nicolosi.user.repository.IUserRepository;
import it.booklovers.nicolosi.user.repository.IUserXLibriRepository;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.googleapis.services.AbstractGoogleClientRequest;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.*;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volume2;
import com.google.api.services.books.model.Volumes;


@Service
public class LibriService implements ILibriService {
	
	@Autowired
	private IUserXLibriRepository userLibriRepository;
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private ILibriRepository libriRepository;
	
	@Autowired
	private GoogleProperties googleProperties;
	
	private ModelMapper getModelMapper() {
		ModelMapper modelMapper = new ModelMapper();
		
		modelMapper.typeMap(UserXLibri.class, LibroDTO.class).addMappings(mapper -> {	
			mapper.map(src -> src.getLibri().getTitolo(), LibroDTO::setTitolo);
			mapper.map(src -> src.getLibri().getAutore(), LibroDTO::setAutore);
			mapper.map(src -> src.getLibri().getDescrizione(), LibroDTO::setDescrizione);
			mapper.map(src -> src.getLibri().getCover(), LibroDTO::setCover);
			mapper.map(src -> src.getLibri().getTotPagine(), LibroDTO::setTotPagine);
			mapper.map(src -> src.getLibri().getGoogleBooksId(), LibroDTO::setGoogleBooksId);
		});
		
		return modelMapper;
	}
	
	private Books getBookClient() throws BaseException{
		try {
			return new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), JacksonFactory.getDefaultInstance(), null)
						    .setGoogleClientRequestInitializer(new BooksRequestInitializer(googleProperties.getApiKey()))
					        .setApplicationName("BooksLovers")
					        .build();
		} catch (GeneralSecurityException | IOException ex) {
			throw new GoogleBooksSearchException();
		}
	}
	
	@Override
	public LibroDTO getUserLibro(String username, String googleBooksId) throws BaseException{
		Optional<UserXLibri> userLibroCheck = userLibriRepository.findByUser_UsernameAndLibri_GoogleBooksId(username, googleBooksId);
		LibroDTO libro = null;
		if(!userLibroCheck.isPresent()) {
			libro = new LibroDTO(Boolean.FALSE);			
		}
		else {
			ModelMapper mapper = new ModelMapper();
			libro = mapper.map(userLibroCheck.get(), LibroDTO.class);
		}
		
		return  libro;
	}

	@Override
	public PagedResultDTO<LibroDTO> getUserLibri(String username, Integer page, Integer size, String sortBy, String sortOrder, UserLibriOption option) throws BaseException{		
		//Pagino i risultati della ricerca
		Pageable pageable = PageRequest.of(page, size, Sort.by(Sort.Direction.fromString(sortOrder),"libri_"+sortBy));
		Page<UserXLibri> libri = null;
					
		//In base all'url determino in base a quale flag filtrare i risultati
		switch(option) {
			case PREFERITI: libri = userLibriRepository.findAllByIsPreferitoAndUser_Username(Boolean.TRUE, username, pageable);break;
			case WISHLIST: libri = userLibriRepository.findAllByIsWishlistAndUser_Username(Boolean.TRUE, username, pageable);break;
			case LETTO: libri = userLibriRepository.findAllByIsLettoAndUser_Username(Boolean.TRUE, username, pageable);break;
			case IN_LETTURA: libri = userLibriRepository.findAllByIsInLetturaAndUser_Username(Boolean.TRUE, username, pageable);break;
		}

		//Mappo i risultati della ricerca nei rispettivi DTO e poi popolo il paged result 
		List<LibroDTO> elements = getModelMapper().map(libri.getContent(), new TypeToken<List<LibroDTO>>() {}.getType());
		
		return new PagedResultDTO<LibroDTO>(elements, libri.getTotalElements(), libri.getTotalPages());
	}
	
	@Override
	public PagedResultDTO<Volume> searchBooks(String query, Long page, Long size) throws BaseException{		
		try {
			Books books = getBookClient();
			
			//Effettuo una ricerca impostando un massimo di risultati e l'elemento da cui partire a recuperare
			Books.Volumes.List volumesList = books.volumes().list(query)
															.setMaxResults(size)
															.setStartIndex(page*size);
			Volumes volumes = volumesList.execute();
										
			//Imposto il massimo risultato in base al minimo tra i due valori (workaround per problemi di feth dei libri)			
			Long totalItems = Long.valueOf(Math.min(volumes.getTotalItems(), GOOGLE_BOOKS_RESULT_LIMIT));
			
			//Calcolo le pagine totali
			Double totalPages = Math.ceil(Double.valueOf(totalItems)/Double.valueOf(size));
			
			return new PagedResultDTO<Volume>(volumes.getItems(), totalItems, totalPages.intValue());
		} catch (IOException ex) {
			throw new GoogleBooksSearchException(query);
		}
	}

	@Override
	public Volume getBook(String googleBookId) throws BaseException {
		try {
			Books books = getBookClient();
			
			Books.Volumes.Get volumeGet = books.volumes().get(googleBookId);
			Volume volume = volumeGet.execute();											
			
			return volume;
		} catch (IOException ex) {
			throw new GoogleBooksSearchException();
		}
	}

	@Override
	public void addUserPreference(String username, LibroDTO libroDto, UserLibriOption option, Boolean preference) throws BaseException {
		UserXLibri userLibro = null;
		
		Optional<UserXLibri> userLibroCheck = userLibriRepository.findByUser_UsernameAndLibri_GoogleBooksId(username, libroDto.getGoogleBooksId());
		
		if(!userLibroCheck.isPresent()) {
			Optional<User> user = userRepository.findByUsername(username);
			
			if(!user.isPresent()) throw new AddPreferenceException();
			
			Optional<Libri> libroOpt = libriRepository.findByGoogleBooksId(libroDto.getGoogleBooksId());
			Libri libro = null;
			
			if(!libroOpt.isPresent()) {
				ModelMapper mapper = new ModelMapper();
				mapper.typeMap(LibroDTO.class, Libri.class).addMappings(map -> {					
					map.skip(Libri::setId);
				});
				Libri libroEntity = mapper.map(libroDto, Libri.class);
				
				libro = libriRepository.save(libroEntity);
			}
			else {
				libro = libroOpt.get();
			}
			
			userLibro = new UserXLibri();
			userLibro.setUser(user.get());
			userLibro.setLibri(libro);
		}
		else {		
			userLibro = userLibroCheck.get();
		}		

		userLibro.setCurrentPage(libroDto.getCurrentPage());
		
		switch(option) {
			case PREFERITI: userLibro.setIsPreferito(preference);break;
			case WISHLIST: userLibro.setIsWishlist(preference);break;
			case LETTO: userLibro.setIsLetto(preference);break;
			case IN_LETTURA: userLibro.setIsInLettura(preference);break;
		}		
		
		userLibriRepository.save(userLibro);
		
	}
}	
