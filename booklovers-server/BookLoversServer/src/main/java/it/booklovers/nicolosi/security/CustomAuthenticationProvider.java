package it.booklovers.nicolosi.security;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.user.dto.LoginDTO;
import it.booklovers.nicolosi.user.repository.IUserRepository;

@Component
public class CustomAuthenticationProvider implements AuthenticationProvider {
 
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private PasswordEncoder passwordEncoder;
	
    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        LoginDTO credential = (LoginDTO) authentication.getPrincipal();
        
        //Verifico se lo username e la password sono popolati
        if(credential.getUsername() != null && credential.getPassword() != null){
        
        	//Verifico se esiste un utente con lo username utilizzato per il login
        	Optional<User> user = userRepository.findByUsername(credential.getUsername());
        	
        	//Verifico se la password utilizzata è corretta
        	if(user.isPresent() && passwordEncoder.matches(credential.getPassword(), user.get().getPassword())) {
        		return new CustomUsernamePasswordAuthToken(credential.getUsername(), passwordEncoder.encode(credential.getPassword()), user.get().getUsername());
        	}        	        	
        }        
        
        return null;
    }
 
    @Override
    public boolean supports(Class<?> authentication) {
        return authentication.equals(CustomUsernamePasswordAuthToken.class);
    }

}