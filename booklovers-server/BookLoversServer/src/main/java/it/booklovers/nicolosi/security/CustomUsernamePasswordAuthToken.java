package it.booklovers.nicolosi.security;

import java.util.Collection;

import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;

import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.user.dto.UserDTO;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class CustomUsernamePasswordAuthToken extends UsernamePasswordAuthenticationToken implements Authentication{
    
    private String username;
    
    public CustomUsernamePasswordAuthToken(Object principal, Object credentials, String username) {
        super(principal, credentials);
        this.username = username;
    }
    
    public CustomUsernamePasswordAuthToken(Object principal, Object credentials, Collection<? extends GrantedAuthority> authorities, String username) {
        super(principal, credentials, authorities);
        this.username = username;
    }    
}
