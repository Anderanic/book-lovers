package it.booklovers.nicolosi.security;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import com.fasterxml.jackson.databind.ObjectMapper;

import it.booklovers.nicolosi.common.constants.Constants;
import it.booklovers.nicolosi.common.dto.ErrorResponse;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.security.service.TokenAuthenticationService;
import it.booklovers.nicolosi.user.dto.LoginDTO;
import it.booklovers.nicolosi.user.dto.UserDTO;

import org.springframework.http.HttpStatus;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.AbstractAuthenticationProcessingFilter;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;

import javax.servlet.FilterChain;
import javax.servlet.http.HttpSession;

public class JWTLoginFilter extends AbstractAuthenticationProcessingFilter {

    private static final Logger logger = LoggerFactory.getLogger(JWTLoginFilter.class);
    
    private ObjectMapper mapper = new ObjectMapper();

    public JWTLoginFilter(String url, AuthenticationManager authManager) {
        super(new AntPathRequestMatcher(url));
        setAuthenticationManager(authManager);
    }

    @Override
    public Authentication attemptAuthentication(HttpServletRequest req, HttpServletResponse res) throws IOException, ServletException {
        LoginDTO creds = new ObjectMapper().readValue(req.getInputStream(), LoginDTO.class);
        
        //Verifico se le credenziali utilizzate sono corrette
        Authentication auth = getAuthenticationManager().authenticate(new CustomUsernamePasswordAuthToken(creds, null, null));
        
        //Inserisco i dati dell'utente in sessione
        HttpSession session = req.getSession();
        String username = ((CustomUsernamePasswordAuthToken) auth).getUsername();            
        session.setAttribute(Constants.USER_SESSION_ATTR, username);

        return auth;
    }

    @Override
    protected void successfulAuthentication(HttpServletRequest req, HttpServletResponse res, FilterChain chain, Authentication auth) throws IOException, ServletException {
        TokenAuthenticationService.addAuthentication(res, auth.getName());
    }
    
    @Override
    protected void unsuccessfulAuthentication(HttpServletRequest request, HttpServletResponse response, AuthenticationException failed) throws IOException, ServletException {    	
    	response.setStatus(HttpStatus.FORBIDDEN.value());
        ErrorResponse errorResponse = new ErrorResponse(request.getRequestURI(), HttpStatus.FORBIDDEN.value(), "Wrong username or password!");

        response.getOutputStream().println(mapper.writeValueAsString(errorResponse));	
    }
}