package it.booklovers.nicolosi.security;

import java.util.Arrays;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.access.AccessDeniedHandler;
import org.springframework.security.web.authentication.AuthenticationFailureHandler;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.logout.HttpStatusReturningLogoutSuccessHandler;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@Configuration
@EnableWebSecurity
@EnableWebMvc
public class WebSecurityConfig extends WebSecurityConfigurerAdapter {

	@Autowired
    CustomAuthenticationProvider authProvider;
	
      @Override
      protected void configure(HttpSecurity http) throws Exception {
        http.cors().and().csrf().disable()
	        .authorizeRequests()
	        //Impedisco di richiamare API diverse da quelle specifica a meno di autenticazione
	        .antMatchers(HttpMethod.POST, "/user/login").permitAll()
	        .antMatchers(HttpMethod.POST, "/user/registration").permitAll()
	        .antMatchers(HttpMethod.POST, "/user/password/**").permitAll()
	        .antMatchers(HttpMethod.GET, "/user/check/**").permitAll()
	        .anyRequest().authenticated()
	        .and()
	        //Aggiunto l'url di login
	        .addFilterBefore(new JWTLoginFilter("/user/login", authenticationManager()), UsernamePasswordAuthenticationFilter.class)
	        .addFilterBefore(new JWTAuthenticationFilter(),UsernamePasswordAuthenticationFilter.class)    	       
	        //Aggiungo l'url di logout
	        .logout().logoutSuccessHandler((new HttpStatusReturningLogoutSuccessHandler(HttpStatus.OK)))
	        .clearAuthentication(true)
	        .invalidateHttpSession(true);
      }
      
      @Override
      public void configure(WebSecurity web) throws Exception {
          web.ignoring().antMatchers(
              "/v2/api-docs",     
              "/swagger-resources/**",  
              "/swagger-ui.html",     
              "/webjars/**");
      }

      @Override
      protected void configure(AuthenticationManagerBuilder auth) throws Exception {
         auth.authenticationProvider(authProvider);
      }
      
      @Bean
      CorsConfigurationSource corsConfigurationSource() {
    	//Creo le configurazioni cors per permettere ad angular di contattare il server
        CorsConfiguration configuration = new CorsConfiguration();
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();

        configuration.setAllowCredentials(true);
        configuration.addAllowedOrigin("*");
        configuration.addAllowedHeader("*");
        configuration.setAllowedMethods(Arrays.asList("GET", "POST", "PUT"));
        configuration.setExposedHeaders(Arrays.asList("authtoken", "Set-Cookie"));
        
        source.registerCorsConfiguration("/**", configuration);
        return source;
      }
      
      @Bean
      public PasswordEncoder passwordEncoder() {
          return new BCryptPasswordEncoder();
      }           
}


