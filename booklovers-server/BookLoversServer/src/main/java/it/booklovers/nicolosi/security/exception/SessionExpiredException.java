package it.booklovers.nicolosi.security.exception;

import org.springframework.http.HttpStatus;

import it.booklovers.nicolosi.common.exception.BaseException;

public class SessionExpiredException extends BaseException {
	public SessionExpiredException() {
		this.status = HttpStatus.UNAUTHORIZED;
		this.err = "Session Expired! Please Sign In again.";
	}
}
