package it.booklovers.nicolosi.user.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import it.booklovers.nicolosi.common.controller.BaseController;
import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.user.dto.UserDTO;
import it.booklovers.nicolosi.user.service.IUserService;

@RestController
@RequestMapping("user")
public class UserController extends BaseController {

	@Autowired
	private IUserService userService;
	
	@RequestMapping(path = "/registration", method = RequestMethod.POST)
	@ResponseBody
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public UserDTO registerUser(HttpServletRequest request,@Valid @RequestBody UserDTO userDTO) throws BaseException {
		return this.userService.registerUser(userDTO);
	}
	
	@RequestMapping(path = "/check/username", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public Boolean checkUsername(HttpServletRequest request,@RequestParam String username) throws BaseException {
		return this.userService.checkUsername(username);
	}
	
	@RequestMapping(path = "/check/email", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public Boolean checkEmail(HttpServletRequest request,@RequestParam String email) throws BaseException {
		return this.userService.checkEmail(email);
	}
	
	@RequestMapping(path = "/logged/info", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public UserDTO getSessionUserInfo(HttpSession session) throws BaseException {
		return userService.getUserInfo(getUsernameFromSession(session));		
	}
	
	@RequestMapping(path = "/info", method = RequestMethod.GET)
	@ResponseBody
    @Produces(MediaType.APPLICATION_JSON)
	public UserDTO getUserInfo(@RequestParam String username) throws BaseException {
		return userService.getUserInfo(username);
	}
	
	@RequestMapping(path = "/password/recovery", method = RequestMethod.POST)
	@ResponseBody
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public void recoveryPassword(@RequestBody UserDTO userDTO) throws BaseException {
		userService.sendRecoveryEmail(userDTO.getEmail());
	}
	
	@RequestMapping(path="/password/reset/{uuid}", method = RequestMethod.POST)
	@ResponseBody
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
	public void resetPassword(@Valid @RequestBody UserDTO.Password passwordDTO, @PathVariable("uuid") String uuid) throws BaseException {
		userService.resetPassword(passwordDTO, uuid);
	}
}
