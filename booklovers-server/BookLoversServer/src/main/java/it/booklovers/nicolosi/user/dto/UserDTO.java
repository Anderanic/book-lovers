package it.booklovers.nicolosi.user.dto;

import java.util.Date;
import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModel;
import it.booklovers.nicolosi.libri.dto.LibroDTO;
import it.booklovers.nicolosi.validator.PasswordMatches;
import it.booklovers.nicolosi.validator.ValidEmail;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@PasswordMatches
@ApiModel
public class UserDTO {
	private Integer id;
	
	@NotNull
	@NotEmpty
	private String nome;
	
	@NotNull
	@NotEmpty
	private String cognome;
	
	@NotNull
	@NotEmpty
	private String username;
	
	@NotNull
	@NotEmpty
	@ValidEmail
	private String email;	
	
	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")	
	private Date dataCreazione;
	
	@NotNull
	private Password passwords;	
	
	@Getter
	@Setter
	@PasswordMatches
	public static class Password{
		@NotNull
		@NotEmpty
		private String password;
		
		@NotNull
		@NotEmpty
		private String matchingPassword;
	}
}
