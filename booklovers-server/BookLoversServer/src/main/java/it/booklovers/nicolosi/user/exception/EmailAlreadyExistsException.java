package it.booklovers.nicolosi.user.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class EmailAlreadyExistsException extends BaseException {
	public EmailAlreadyExistsException() {
		this.err = "Email already in use";
	}
}
