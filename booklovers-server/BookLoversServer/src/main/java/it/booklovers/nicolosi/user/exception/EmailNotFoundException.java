package it.booklovers.nicolosi.user.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class EmailNotFoundException extends BaseException {
	public EmailNotFoundException() {
		this.err = "No user found associated with this email!";
	}
}
