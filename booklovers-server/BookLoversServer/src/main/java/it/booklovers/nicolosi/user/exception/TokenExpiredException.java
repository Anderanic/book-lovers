package it.booklovers.nicolosi.user.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class TokenExpiredException extends BaseException {
	public TokenExpiredException() {
		this.err = "Token has expired!";
	}
}
