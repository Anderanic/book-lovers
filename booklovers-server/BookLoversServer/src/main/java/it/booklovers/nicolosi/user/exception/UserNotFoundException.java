package it.booklovers.nicolosi.user.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class UserNotFoundException extends BaseException {
	public UserNotFoundException() {
		this.err = "User not found!";
	}
}
