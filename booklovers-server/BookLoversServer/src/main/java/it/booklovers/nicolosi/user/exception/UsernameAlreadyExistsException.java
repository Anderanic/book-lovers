package it.booklovers.nicolosi.user.exception;

import it.booklovers.nicolosi.common.exception.BaseException;

public class UsernameAlreadyExistsException extends BaseException{
	public UsernameAlreadyExistsException() {
		this.err = "Username already exists";
	}
}
