package it.booklovers.nicolosi.user.repository;

import java.util.Date;
import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.booklovers.nicolosi.entity.Token;

@Repository
public interface ITokenRepository  extends CrudRepository<Token, Integer>{
	public Optional<Token> findByUuidAndDataScadenzaGreaterThanAndActive(String uuid, Date dataScadenza, Boolean active);
	public Optional<Token> findByUuid(String uuid);
}
