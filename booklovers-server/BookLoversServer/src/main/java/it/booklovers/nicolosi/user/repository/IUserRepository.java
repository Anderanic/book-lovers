package it.booklovers.nicolosi.user.repository;

import java.util.Optional;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.booklovers.nicolosi.entity.User;

@Repository
public interface IUserRepository extends CrudRepository<User, Integer> {
	public Optional<User> findByEmail(String email);
	public Optional<User> findByUsername(String username);
	public Optional<User> findByUsernameAndPassword(String username, String password);
}
