package it.booklovers.nicolosi.user.repository;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import it.booklovers.nicolosi.entity.UserXLibri;

@Repository
public interface IUserXLibriRepository extends CrudRepository<UserXLibri, Integer> {
	public Page<UserXLibri> findAllByIsPreferitoAndUser_Username(Boolean isPreferito, String username, Pageable pagable);
	public Page<UserXLibri> findAllByIsWishlistAndUser_Username(Boolean isWishlist, String username, Pageable pagable);
	public Page<UserXLibri> findAllByIsLettoAndUser_Username(Boolean isLetto, String username, Pageable pagable);
	public Page<UserXLibri> findAllByIsInLetturaAndUser_Username(Boolean isInLettura, String username, Pageable pagable);
	public Optional<UserXLibri> findByUser_UsernameAndLibri_GoogleBooksId(String username, String googleBooksId);
}
