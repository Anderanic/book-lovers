package it.booklovers.nicolosi.user.service;

import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.user.dto.UserDTO;

public interface IUserService {
	public UserDTO registerUser(UserDTO userDTO) throws BaseException;
	public Boolean checkUsername(String username) throws BaseException;
	public Boolean checkEmail(String email) throws BaseException;
	public UserDTO getUserInfo(String username) throws BaseException;
	public void sendRecoveryEmail(String email) throws BaseException;
	public void resetPassword(UserDTO.Password passwordDTO, String uuid) throws BaseException;
}
