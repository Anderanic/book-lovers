package it.booklovers.nicolosi.user.service;

import java.util.Calendar;
import java.util.Date;
import java.util.Optional;
import java.util.UUID;

import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import it.booklovers.nicolosi.common.exception.BaseException;
import it.booklovers.nicolosi.email.service.IEmailService;
import it.booklovers.nicolosi.entity.Token;
import it.booklovers.nicolosi.entity.User;
import it.booklovers.nicolosi.user.dto.UserDTO;
import it.booklovers.nicolosi.user.dto.UserDTO.Password;
import it.booklovers.nicolosi.user.exception.EmailAlreadyExistsException;
import it.booklovers.nicolosi.user.exception.EmailNotFoundException;
import it.booklovers.nicolosi.user.exception.TokenExpiredException;
import it.booklovers.nicolosi.user.exception.UserNotFoundException;
import it.booklovers.nicolosi.user.exception.UsernameAlreadyExistsException;
import it.booklovers.nicolosi.user.repository.ITokenRepository;
import it.booklovers.nicolosi.user.repository.IUserRepository;


@Service
public class UserService implements IUserService {
	
	private static final Logger logger = LoggerFactory.getLogger(UserService.class);

	@Autowired
	private PasswordEncoder passwordEncoder;
	
	@Autowired
	private IUserRepository userRepository;
	@Autowired
	private ITokenRepository tokenRepository;
	
	@Autowired
	private IEmailService emailService;
	
	@Override
	public UserDTO registerUser(UserDTO userDTO) throws BaseException {
		//Verifico se l'email è già utilizzata
		if(this.checkEmail(userDTO.getEmail())) throw new EmailAlreadyExistsException();
		
		//Verifico se lo username è già utilizzato
		if(this.checkUsername(userDTO.getUsername())) throw new UsernameAlreadyExistsException();
		
		ModelMapper modelMapper = new ModelMapper();
		
		//Cripto la password, genero il codice amico ed inserisco la data di creazione
		modelMapper.typeMap(UserDTO.class, User.class).addMappings(mapper -> {
			mapper.map(src -> passwordEncoder.encode(userDTO.getPasswords().getPassword()), User::setPassword);
			mapper.map(src -> Long.toHexString(new Date().getTime()), User::setCodiceAmico);
			mapper.map(src -> new Date(), User::setDataCreazione);
		});
		
		//Mappo i dati del dto nell'ORM
		User user = modelMapper.map(userDTO, User.class);
		
		userDTO.setId(userRepository.save(user).getId());		
		
		return userDTO;
	}

	@Override
	public Boolean checkUsername(String username) throws BaseException {
		return userRepository.findByUsername(username).isPresent();
	}
	
	@Override
	public Boolean checkEmail(String email) throws BaseException {
		return userRepository.findByEmail(email).isPresent();
	}

	@Override
	public UserDTO getUserInfo(String username) throws BaseException {
		Optional<User> user = userRepository.findByUsername(username);
		
		//Verifico se l'utente esiste
		if(!user.isPresent()) throw new UserNotFoundException();				
		
		ModelMapper modelMapper = new ModelMapper();
		UserDTO userDTO = modelMapper.map(user.get(), UserDTO.class);
		
		return userDTO;
	}

	@Override
	public void sendRecoveryEmail(String email) throws BaseException {
		Optional<User> user = userRepository.findByEmail(email);
		
		if(!user.isPresent()) throw new EmailNotFoundException();
		
		Calendar calendar = Calendar.getInstance();
		calendar.add(Calendar.DAY_OF_MONTH, 1);
		
		Token token = new Token(user.get(), UUID.randomUUID().toString(), calendar.getTime());
		
		tokenRepository.save(token);
		
		emailService.sendResetPasswordEmail(email, token.getUuid());
	}

	@Override
	public void resetPassword(Password passwordDTO, String uuid) throws BaseException {
		Optional<Token> token = tokenRepository.findByUuidAndDataScadenzaGreaterThanAndActive(uuid, new Date(), Boolean.TRUE);
		
		if(!token.isPresent()) throw new TokenExpiredException();
		
		token.get().getUser().setPassword(passwordEncoder.encode(passwordDTO.getPassword()));
		token.get().setActive(Boolean.FALSE);
		
		tokenRepository.save(token.get());
	}
}
