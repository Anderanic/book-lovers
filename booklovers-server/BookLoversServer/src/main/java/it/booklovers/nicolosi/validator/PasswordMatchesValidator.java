package it.booklovers.nicolosi.validator;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import it.booklovers.nicolosi.user.dto.UserDTO;

public class PasswordMatchesValidator implements ConstraintValidator<PasswordMatches, Object> { 
   
  @Override
  public void initialize(PasswordMatches constraintAnnotation) {}
  
  @Override
  public boolean isValid(Object obj, ConstraintValidatorContext context){   
	  if(obj instanceof UserDTO) {
		  UserDTO user = (UserDTO) obj;      
	      return user.getPasswords().getPassword().equals(user.getPasswords().getMatchingPassword());
	  }
	  else{
		  UserDTO.Password passwords = (UserDTO.Password) obj;      
	      return passwords.getPassword().equals(passwords.getMatchingPassword());
	  }
  }     
}